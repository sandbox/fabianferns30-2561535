CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------
The Variable editor module provides a UI for editing, editing or deleting any
 global variable.
There is a one click submit for saving array as well as any other variable
 type. There is a ctool popup that helps to add new variables
The project provides root user to edit all kind of variables. Other users can 
define their own variable and edit them.


 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/fabianferns30/2561535
   


 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/node/2561547



REQUIREMENTS
------------
This module requires the following modules:
 * Ctools (https://www.drupal.org/project/ctools)

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------
 * Configure user permissions in Administration » People » Permissions:


   - set the permission for user to use the link
   - you can view the link in Administration » Configuration » Development

TROUBLESHOOTING
---------------
 * If the menu link does not display, check the following:


   - Are the "Variable editor form" permissions enabled for the appropriate
     roles?

MAINTAINERS
-----------
Current maintainers:
 * Fabian Fernandes (fabian.fernandes_30) - https://www.drupal.org/user/3046083
 * pritika agrawal (nitinpritika) - https://www.drupal.org/user/2495500
 * A AjayKumar Reddy (ajaykumarreddy1392) - https://www.drupal.org/user/3261994
